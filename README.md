**[EN below](#data-cleaning-with-probabilistic-programming)**

# Čištění dat pomocí pravděpodobnostního programování

**Vedoucí** - Mgr. Vojtěch Rybář

**Oponent** - Ing. Daniel Vašata, Ph.D.

**Typ práce** - Bakalářská práce

**Jazyk práce** - Čeština

#### Abstrakt

Práce zahrnuje rešerši z oblasti čištění a doplňování datových sad a zaměřuje se na konkrétní přístup s využitím pravděpodobnostního programování. Pro praktickou část práce je využíván pravděpodobnostní programovací jazyk PClean naprogramovaný v jazyce Julia. Jsou vysvětleny principy, na kterých funguje, a popsány konkrétní části nutné pro sepsání programu. Následně je v PClean napsán program pro doplnění a opravu hodnot v datové sadě se záznamy automobilů (cena, výkon, palivo, atd.)

S touto opravenou sadou dat se provádí odhad ceny za pomocí regrese a kvalita výsledku se porovnává s výsledky při použití neupravených dat se sjednocenou doplňovanou hodnotou pro každý sloupec, anebo doplněných na základě odborných znalostí. Model naučený daty doplněných s PClean nedosahuje kvalit modelu založeného na odborných znalostech. Ale na druhou stranu PClean nabízí rychlý způsob doplnění chybějících kategorických hodnot s kvalitou přesahující dnes běžně používané triviální doplnění.

#### Klíčová slova

PClean, Julia, pravděpodobnostní programování, příprava dat, čištění datových sad, doplňování chybějících dat

#### Zadání

Čištění dat je důležitou, ale často opomíjenou součástí správného pracovního postupu v oblasti datových věd. V případě aplikací umělé inteligence ve vysoce rizikových prostředích, jako jsou zdravotnictví a finance, by předpovědi provedené modely trénovanými na problematických datech mohly v reálných aplikacích způsobit značné škody.

PClean je pravděpodobnostní programovací nástroj, který umožňuje využívat znalosti specifické pro danou oblast k čištění a normalizaci problematických datových sad.

V této práci bude podán přehled současných přístupů k čištění dat, vysvětleny principy nástroje PClean, tento nástroj aplikován na datovou sadu z reálného světa a porovnána výkonnost modelů strojového učení natrénovaných na původní datové sadě a datové sadě vyčištěné pomocí PClean.


# Data cleaning with probabilistic programming

**Supervisor** - Mgr. Vojtěch Rybář

**Reviewer** - Ing. Daniel Vašata, Ph.D.

**Thesis type** - Bachelor thesis

**Thesis language** - Czech

#### Abstract

This paper includes research in the field of cleaning and filling in datasets and focuses on a specific approach using probabilistic programming. The practical part of the work operates with the probabilistic programming language PClean, programmed in Julia. The principles on which it operates are explained and the specific parts required to write the program are laid out. Subsequently, PClean is used to write a program for filling and correcting values in a data set with car records (price, power, fuel, etc.)

Once this dataset is corrected, regression is used to estimate the price and the quality of the result is compared with the results based on uncorrected data with standardised added values for each column or added based on expert knowledge. The model learned the data via PClean does not achieve the qualities of the model based on expert knowledge. However, PClean does offer a fast way to fill in missing categorical values with a quality exceeding the trivial fill-in mechanism commonly used today.

#### Keywords

PClean, Julia, probabilistic programming, data preprocessing, data cleaning, filling missing data

