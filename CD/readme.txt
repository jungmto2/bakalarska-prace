Obsah pÅiloÅ¾enÃ©ho mÃ©dia

- mail.txt - kopie emailu od Alexandera Lewa
- readme.txt - struÄnÃ½ popis obsahu mÃ©dia
- bachelor thesis - sloÅ¾ka s textem prÃ¡ce
	- thesis.pdf - text prÃ¡ce ve formÃ¡tu PDF
- src - zdrojovÃ© kÃ³dy
	- edited official experiments - sloÅ¾ka s upravenÃ½mi oficiÃ¡lnÃ­mi experimenty
	- experiments - zdrojovÃ© kÃ³dy prezentovanÃ© v textu
		- Erased10percent.ipynb - celÃ½ kÃ³d k experimentu 3
		- Filling3columns.ipynb - kÃ³d prezentovanÃ½ v dodatku B
	- LaTeX - zdrojovÃ¡ forma prÃ¡ce ve formÃ¡tu LaTeX

