using PClean


#include("load_data.jl")
# ------------------------------ load_data.jl ------------------------------
using CSV   # import
using DataFrames: DataFrame   # import

# Load data
dataset = "rents"
dirty_table = CSV.File("datasets/$(dataset)_dirty.csv", stringtype=String) |> DataFrame
clean_table = CSV.File("datasets/$(dataset)_clean.csv", stringtype=String) |> DataFrame

# dirty_table[!, :CountyKey] je totožné jako dirty_table.CountKey (přímý přístup bez kopie)
# vytvoření sloupce 'CountyKey' s 2 písmenky -> první a poslední písmenko prvního slova v položce 'County'
dirty_table[!, :CountyKey] = map(x -> "$(x[1])$(split(x)[1][end])", dirty_table[!, :County])

# vytvoření slovníku všech unikátních 'CountyKey' a následně přidáním jako položky všechny původní sousloví
possibilities = Dict(c => Set() for c in unique(dirty_table.CountyKey))
for r in eachrow(dirty_table)
  push!(possibilities[r[:CountyKey]], r[:County])
end
possibilities = Dict(c => [possibilities[c]...] for c in keys(possibilities))

# soupis všech států do vektoru z datasetu
const states = unique(filter(x -> !ismissing(x), dirty_table.State))
# soupis všech typů bytu z výpisu
const room_types = ["studio", "1br", "2br", "3br", "4br"]
# ------------------------------ load_data.jl ------------------------------


# dvouprvkový vektor Gausovských transformací (forward, backward, deriv)
units = [Transformation(identity, identity, x -> 1.0),
        Transformation(x -> x/1000.0, x -> x*1000.0, x -> 1/1000.0)]

# PClean Class
PClean.@model RentsModel begin
  @class County begin
    @learned state_pops::ProportionsParameter
    countykey ~ Unmodeled()
    @guaranteed countykey
    name ~ StringPrior(10, 35, possibilities[countykey])
    state ~ ChooseProportionally(states, state_pops)
  end;

  @class Obs begin
    @learned avg_rent::Dict{String, MeanParameter{1500, 1000}}
    county ~ County
    county_name ~ AddTypos(county.name, 2)
    br ~ ChooseUniformly(room_types)
    unit ~ ChooseUniformly(units)
    rent_base = avg_rent["$(county.state)_$(county.countykey)_$(br)"]
    rent ~ TransformedGaussian(rent_base, 150.0, unit)
    corrected = round(unit.backward(rent))
  end;
end;

# PClean query
query = @query RentsModel.Obs [
  CountyKey      county.countykey
  County         county.name      county_name
  State          county.state
  "Room Type"    br
  "Monthly Rent" corrected        rent
];

println("data import completed")

# Configuration
config = PClean.InferenceConfig(1, 2; use_mh_instead_of_pg=true, rejuv_frequency=500)
# Configure observed dataset
observations = [ObservedDataset(query, dirty_table)]

@time begin
  # SMC initialization
  trace = initialize_trace(observations, config);
  # Rejuvenation sweeps
  run_inference!(trace, config);
end
# 31.264035 seconds (284.78 M allocations: 10.996 GiB, 11.09% gc time, 6.68% compilation time)

# v rámci výsledků je uložen i opravený dataset !!!
results = evaluate_accuracy(dirty_table, clean_table, trace.tables[:Obs], query)
PClean.save_results("results", "rents", trace, observations)
println(results)
