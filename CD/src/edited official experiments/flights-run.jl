using PClean

#include("load_data.jl")
# ------------------------------ load_data.jl ------------------------------
using CSV   # import
using DataFrames: DataFrame   # import

# Load data
dataset = "flights"
dirty_table = CSV.File("datasets/$(dataset)_dirty.csv", stringtype=String) |> DataFrame
clean_data = CSV.File("datasets/$(dataset)_clean.csv", stringtype=String) |> DataFrame

# vytvoření klíčů ve slovníku ve formátu označení letu + jeden ze 4 nadpisů sloupců pro časový údaj (100 označení * 4 časy)
times_for_flight = Dict{String, Set{String}}("$fl-$field" => Set() for fl in unique(dirty_table.flight) for field in [:sched_dep_time, :sched_arr_time, :act_dep_time, :act_arr_time])
# dodání hodnot časů k danému záznamu ve slovníku
for row in eachrow(dirty_table)
  for field  in [:sched_dep_time, :sched_arr_time, :act_dep_time, :act_arr_time]
    !ismissing(row[field]) && push!(times_for_flight["$(row[:flight])-$field"], row[field])
  end
end

times_for_flight = Dict(fl => [unique(times_for_flight[fl])...] for fl in keys(times_for_flight))
# uložení všech 100 označení pro let
flight_ids = unique(dirty_table.flight)
# ------------------------------ load_data.jl ------------------------------

# uložení 38 zdrojů
websites = unique(dirty_table.src)

# PClean class
PClean.@model FlightsModel begin
  @class TrackingWebsite begin
    name ~ StringPrior(2, 30, websites)
  end
  
  @class Flight begin
    begin 
      flight_id ~ StringPrior(10, 20, flight_ids); @guaranteed flight_id
    end
    sdt ~ TimePrior(times_for_flight["$flight_id-sched_dep_time"])
    sat ~ TimePrior(times_for_flight["$flight_id-sched_arr_time"])
    adt ~ TimePrior(times_for_flight["$flight_id-act_dep_time"])
    aat ~ TimePrior(times_for_flight["$flight_id-act_arr_time"])
  end

  @class Obs begin
    @learned error_probs::Dict{String, ProbParameter{10.0, 50.0}}
    begin 
      flight ~ Flight; 
    end
    src ~ TrackingWebsite 
    error_prob = lowercase(src.name) == lowercase(flight.flight_id[1:2]) ? 1e-5 : error_probs[src.name]
    begin
      sdt ~ MaybeSwap(flight.sdt, times_for_flight["$(flight.flight_id)-sched_dep_time"], error_prob)
      sat ~ MaybeSwap(flight.sat, times_for_flight["$(flight.flight_id)-sched_arr_time"], error_prob)
      adt ~ MaybeSwap(flight.adt, times_for_flight["$(flight.flight_id)-act_dep_time"],   error_prob)
      aat ~ MaybeSwap(flight.aat, times_for_flight["$(flight.flight_id)-act_arr_time"],   error_prob)
    end
  end
end;

# PClean query
query = @query FlightsModel.Obs [
  sched_dep_time flight.sdt       sdt
  sched_arr_time flight.sat       sat
  act_dep_time   flight.adt       adt
  act_arr_time   flight.aat       aat
  flight         flight.flight_id
  src            src.name
];

# Configure observed dataset
observations = [ObservedDataset(query, dirty_table)]
# Configuration
config = PClean.InferenceConfig(5, 2; use_mh_instead_of_pg=true)

@time begin 
  # SMC initialization
  tr = initialize_trace(observations, config);
  # Rejuvenation sweeps
  run_inference!(tr, config)
end
# 5.292247 seconds (28.18 M allocations: 1.437 GiB, 6.90% gc time, 40.92% compilation time)

println(evaluate_accuracy(dirty_table, clean_data, tr.tables[:Obs], query))
# (f1 = 0.8840564773781304, errors = 2608, changed = 2471, cleaned = 2213, precision = 0.8967175412920761, recall = 0.8717479674796748, imputed = 2312, correctly_imputed = 2076)
